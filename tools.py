''''
Модуль вспомогательных функций.
'''


def is_lanit_email(email_str):
    '''
    Возвращает True если передан email Ланит
    :param email: Строка email
    :return: Boolean
    '''

    from email.utils import parseaddr

    name, email = parseaddr(email_str)
    print(name, email)
    domain = None
    try:
        domain = email.split('@')[1]
    except Exception:
        pass
    return domain == 'lanit.ru'
