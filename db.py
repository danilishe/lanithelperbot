import sqlite3

class User(object):

    def __init__(self, user_id):
        '''
        Инциализация модели пользователя.
        Тут мы должны подгрузить всю информацию о пользователе из БД.
        На вход конструктора подаётся id пользователя Telegram. (Он же наш user.id)
        '''
        conn = sqlite3.connect("TelegramBot.db")  # Обращение к БД
        cursor = conn.cursor()

        query = 'select * from users where id = "{}"'.format(user_id)

        result = cursor.execute(query)

        rows = cursor.fetchall()

        if not rows:
            # Создание записи в таблице БД с пустым пользователем
            #         id = user_id
            #         mail = None
            #         confirmed = False
            #         confirmed_code = False
            #         status = None
            #         extra_json = ''
            cursor.execute("insert into users (id, mail, confirmed, confirmation_code)"
                           " VALUES ('{}', '{}', '{}', '{}')".format(user_id, '', 'False', ''))
            conn.commit()

            result = {}
            result['id'] = user_id
            result['mail'] = None
            result['confirmed'] = False
            result['confirmation_code'] = False
            result['status'] = None
            result['extra_json'] = ''

        else:
            row = rows[0]
            result = {}
            result['id'] = row[0]
            result['mail'] = row[1]
            result['confirmed'] = row[2] == 'True'
            result['confirmation_code'] = row[3]
            result['status'] = row[4]
            result['extra_json'] = row[5]

        conn.close()

        # Таблица user
        # id,mail,confirmed=True/False,confirmation_code

        # Блок отладки для Кунашева
        # if user_id == 239100928:
        #     result = dict(id=239100928,
        #                   mail=None,
        #                   confirmed=False,
        #                   confirmated_code=False,
        #                   status='',
        #                   extra_json={},
        #                   )
        # else:
        #     # Блок отладки для Остальных
        #     result = dict(id=123,
        #                   mail='kunashev@lanit.ru',
        #                   confirmed=False,
        #                   confirmated_code=12345,
        #                   status='',
        #                   extra_json = {'anything_else': 'You must use NoSQL!'},
        #                   )

        self.id = result['id']
        self.mail = result['mail']
        self.confirmed = result['confirmed']
        self.confirmation_code = result['confirmation_code']
        self.extra_json = result['extra_json']
        self.status = result['status']

    def update(self, field_name, value):
        '''
        Метод обновляет инфу в БД по пользователю
        :param field_name:
        :param value:
        :return: Boolean
        '''
        # Обновление данных о пользователе после отправки кода авторизации на почту
        conn = sqlite3.connect("TelegramBot.db")
        cursor = conn.cursor()
        sql = "UPDATE users SET {} = ('{}') WHERE id = ('{}')".format(field_name, value, self.id)
        cursor.execute(sql)
        conn.commit()
        conn.close()

        return True

    def get_tags(self):
        '''
        Метод возвращает теки пользователя списком.
        Теги это контекст сотрудника (Офис, Отдел, Проекты в которых он участвует
        :return: list
        '''
        # self.id
        # Тут делаешь запрос на текги по айди пользователя
        return ['ООК', 'Ижевск', 'ГИС ЖКХ']

    def is_logged(self):
        '''
        Залогинен ли пользователь. Подверрдил ли он своё мыло Ланит
        :return:
        '''
        return self.confirmed
