FROM stibbons31/alpine-s6-python3-twisted:latest

RUN apk add --no-cache git
RUN git clone https://gitlab.com/danilishe/lanithelperbot.git
WORKDIR /lanithelperbot
RUN git checkout demo
RUN pip3 install python-telegram-bot

ENTRYPOINT ["python3", "main.py"]