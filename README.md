Комплексный бот для телеграмма, предназначенный для решения административных проблем у новых и старых сотрудников Ланит.

# Устновка зависимостей
 - conda install -c conda-forge python-telegram-bot

# Пример запуска под Docker
docker build -t mslacker/mercurial .
docker push mslacker/lanitbot
docker run --rm --name lanitbot -ti -v D:\projs\lanithelperbot\config.py:/lanithelperbot/config.py:ro mslacker/lanitbot