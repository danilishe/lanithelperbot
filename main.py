#!/usr/bin/env python
# -*- coding: utf-8 -*-
# This program is dedicated to the public domain under the CC0 license.

"""
Simple Bot to reply to Telegram messages.
First, a few handler functions are defined. Then, those functions are passed to
the Dispatcher and registered at their respective places.
Then, the bot is started and runs until we press Ctrl-C on the command line.
Usage:
Basic Echobot example, repeats messages.
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""

import secrets
import logging

from telegram.ext import Updater, CommandHandler, MessageHandler, Filters

import db
import auth
import config
import tools


# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)


# Define a few command handlers. These usually take the two arguments update and
# context. Error handlers also receive the raised TelegramError object in error.
def start(update, context):
    """Send a message when the command /start is issued."""
    user = db.User(user_id=update.effective_user.id)
    if user.confirmed:
        update.message.reply_text('Ты зареган. Спрашивай что тебе надо.')
    elif not user.mail:  # если пользователь не начал регистрацию (поле mail пустое)
        update.message.reply_text('Hi! Ты не зарегистрирован укажи свой email в домене @lanit для регистрации.')


def enter_confirm(update, context):
    user = db.User(user_id=update.effective_user.id)
    if user.confirmation_code == update.message.text:
        update.message.reply_text('Код корректный. Ты зареган.')
        user.update('confirmed', 'True')
    else:
        update.message.reply_text('Код не корректный!')
    return


def registration(update, context):
    print(context) # update.effective_user.username

    user = db.User(user_id=update.effective_user.id)

    if not user.confirmed and not user.mail:
        if tools.is_lanit_email(update.message.text):
            # Пользователь ввёл своё мыло
            token = secrets.token_urlsafe(24)[:6]

            msg = 'На почту {} отправлен код подтверждения.\n' \
                  '*В режиме хакатона не все письма доходят, так как smtp блокирует их как спам. ' \
                  'Если письмо непришло используйте код: {}'.format(update.message.text, token)

            user.update('mail', update.message.text)
            user.update('confirmation_code', token)

            update.message.reply_text(msg)
            auth.auth_send(update.message.text, token)
        else:
            update.message.reply_text('Не могу распарсить твой email')
            start(update, context)


def help(update, context):
    """Send a message when the command /help is issued."""
    update.message.reply_text('Help!')


def echo(update, context):
    """Echo the user message."""
    #update.message.reply_text(update.message.text)
    registration(update, context)
    user = db.User(user_id=update.effective_user.id)
    if user.confirmed:
        update.message.reply_text('Ты зареган. Спрашивай что тебе надо.')
    elif user.confirmation_code:
        enter_confirm(update, context)

def error(update, context):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, context.error)


def main():
    """Start the bot."""
    # Create the Updater and pass it your bot's token.
    # Make sure to set use_context=True to use the new context based callbacks
    # Post version 12 this will no longer be necessaryecho
    updater = Updater(config.TOKEN, use_context=True)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("help", help))


    # on noncommand i.e message - echo the message on Telegram
    dp.add_handler(MessageHandler(Filters.text, echo))

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()