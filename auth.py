import smtplib                                              # Импортируем библиотеку по работе с SMTP
import os                                                   # Функции для работы с операционной системой, не зависящие от используемой операционной системы

# Добавляем необходимые подклассы - MIME-типы
import mimetypes                                            # Импорт класса для обработки неизвестных MIME-типов, базирующихся на расширении файла
from email import encoders                                  # Импортируем энкодер
from email.mime.base import MIMEBase                        # Общий тип
from email.mime.text import MIMEText                        # Текст/HTML
from email.mime.image import MIMEImage                      # Изображения
from email.mime.audio import MIMEAudio                      # Аудио
from email.mime.multipart import MIMEMultipart              # Многокомпонентный объект

#####
import string 
import secrets  
####


def send_email(addr_to, msg_subj, msg_text ): # addr_to Получатель msg_subj - Тема сообщения  msg_text - сообщение
    addr_from = "forbot.a@yandex.ru"                  # Адресат
    #addr_to   = "forbot.a@yandex.ru"                 # Получатель
    password  = "forbot.a4454"                        # Пароль Адресата

    msg = MIMEMultipart()                               # Создаем сообщение
    msg['From']    = addr_from                          # Адресат
    msg['To']      = addr_to                            # Получатель
    msg['Subject'] = msg_subj                           # Тема сообщения

    body = msg_text
    msg.attach(MIMEText(body, 'plain'))                 # Добавляем в сообщение текст

    server = smtplib.SMTP_SSL('smtp.yandex.ru', 465)    # Создаем объект SMTP
#server.starttls()                                      # Начинаем шифрованный обмен по TLS
    server.login(addr_from, password)                   # Получаем доступ
    server.send_message(msg)                            # Отправляем сообщение
    server.quit()                                       # Выходим      
    
# отправка при авторизации
def auth_send(addr_to, token):

    msg = "Поздравляю!, вы получили код авторизации:"+token+", отправте его боту"
    send_email(addr_to,"Добро пожаловать!", msg)


#auth_send("forbot.a@yandex.ru")